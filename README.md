# Story 7

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

A simple accordion feature implemented in a simple web

[pipeline-badge]: https://gitlab.com/story-ppw/story-7/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/story-ppw/story-7/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/story-ppw/story-7/-/commits/master
