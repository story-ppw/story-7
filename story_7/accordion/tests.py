from django.test import TestCase
from django.urls import reverse
from django.test import Client

class UnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.response = Client().get(reverse("accordion:index"))

    def test_root_url_status_200(self):
        self.assertEqual(self.response.status_code, 200)

    def test_correct_template_is_used(self):
        self.assertTemplateUsed(self.response, "accordion/index.html")

    def test_about_me_is_in_page(self):
        self.assertContains(self.response, "About Me")

    def test_current_activity_is_in_page(self):
        self.assertContains(self.response, "Current Activity")

    def test_organisation_is_in_page(self):
        self.assertContains(self.response, "Organisation")

    def test_achievement_is_in_page(self):
        self.assertContains(self.response, "Achievement")
