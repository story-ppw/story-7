let header = "";
let description = "";
let form_data = {};

$( function() {
    $( "#accordion" ).on("click", "h1", function() {
        $(this).parent().next().toggle(200);
    });

    $( "#accordion" ).on("click", ".arrowDown", function() {
        let parent = $(this).closest("article");
        parent.insertAfter(parent.next()).hide().show("slow");
    })

    $( "#accordion" ).on("click", ".arrowUp", function() {
        let parent = $(this).closest("article");
        parent.insertBefore(parent.prev()).hide().show("slow");
    })

    $( "#accordion" ).on("click", ".delete", function() {
        let parent = $(this).closest("article")
        parent.hide("medium", function(){ $target.remove(); });
    })

    $( "#add, #close" ).click(function() {
        $( "#modal" ).toggle(200);
    })

    $( "#form" ).submit(function(event) {
        form_data = $(this).serializeArray();
        header = form_data[0].value
        description = form_data[1].value

        // Create new accordion item with template
        let clone = document.querySelector("#accordion-item-template").content.cloneNode(true);
        clone.querySelector("#header").textContent = header
        clone.querySelector("#description p").textContent = description
        $( "#accordion" ).append(clone);

        $('#form').trigger("reset");

        event.preventDefault();
    })
} );
