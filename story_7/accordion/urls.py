from django.urls import path

from .views import index

app_name = 'accordion'

urlpatterns = [
    path('', index, name='index'),
]
